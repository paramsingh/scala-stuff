class Rational( num : Int, den:Int ) {
    def gcd( x: Int, y: Int) : Int = {
        if (x == 0) y
        else if (x < 0) gcd(-x, y)
        else if (y < 0) -gcd(x, -y)
        else gcd(y % x, x)
    }
    val hcf = gcd(num, den)
    val numer : Int = num/hcf
    val denom : Int = den/hcf
    def square = (this * this).toString
    override def toString = numer + "/" + denom
    def +(that: Rational) =
        new Rational(numer*that.denom + that.numer*denom, denom*that.denom)
    def -(that: Rational) =
        new Rational(numer*that.denom - that.numer*denom, denom*that.denom)
    def *(that: Rational) =
        new Rational( numer*that.numer, denom * that.denom)
    def /(that: Rational) =
        new Rational( numer*that.denom, denom * that.numer)
}

/** Write a program to find the sum of 1,1/2,1/3 ... 1/10 in rational form */
var i = 1
var sum = new Rational(1,1)
while( i<=10 ) {
    sum += new Rational(1,i)
    i += 1
}
println("Sum is equal to : "+sum.toString)
println("Square of 1/2 is "+ new Rational(1,2).square)
