def sum(a:Int, b: Int): Int = {
    if( a==b) a
    else a+sum(a+1,b)
}
def sumOfSquares( a:Int, b:Int) : Int = {
    def square(a:Int) = a*a
    if(a==b) square(a)
    else square(a)+ sumOfSquares(a+1,b)
}
def sumPowersOfTwo(a:Int,b:Int) : Int = {
    def powerOfTwo(n:Int):Int = {
        if( n==0 ) 1
        else 2*powerOfTwo(n-1)
    }
    if(a==b) powerOfTwo(a)
    else powerOfTwo(a) + sumPowersOfTwo(a+1,b)
}
def sum(f:Int => Int, a:Int, b:Int ): Int = {
    //sum is a higher order function
    //it takes another function as a parameter
    if(a==b) f(a)
    else f(a)+sum(f,a+1,b)
}
def cube( a:Int) = a*a*a
println(sum(cube,1,4))
println(sum((x => x*x), 1, 4))
