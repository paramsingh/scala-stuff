def sum(f: Int=>Int): (Int, Int)=>Int = {
    def sumF(a:Int, b:Int) : Int = {
        if( a==b)
            f(a)
        else f(a) + sumF(a+1,b)
    }
    sumF
}

println( sum( x=> x*x)(1,3) )
println( sum(x=> x)(1,3) )

val sumInt = sum( x=>x )
val sumSquares = sum( x=> x*x)

def powerOfTwo( x:Int ): Int = {
    if( x==0 ) 1
    else 2 * powerOfTwo(x-1)
}

val sumPowerOfTwo = sum( powerOfTwo )

println(sumInt(1,5))
println(sumSquares(1,4))
println(sumPowerOfTwo(1,4))

def product( f : Int => Int ) : (Int,Int)=>Int = {
    //function similar to sum
    def productF(a: Int, b: Int):Int = {
        if( a==b ) f(a)
        else f(a)*productF(a+1,b)
    }
    productF
}
    
def anyOp( f:Int=>Int,operation: (Int,Int)=>Int): (Int,Int)=>Int = {
    //function to do any operation based on parameter
    def func(a:Int, b:Int) : Int = 
        if(a==b) f(a)
        else operation(f(a),func(a+1,b))
    func
}

val sums = anyOp(x=>x,(x,y)=>x+y)
val products = anyOp(x=>x,(x,y)=>x*y)

println("Generalized sum function : " + sums(1,4))
println("Generalized product function:"+ products(1,4))
