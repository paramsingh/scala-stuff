/** A fixed point of a function is x such that f(x) = x */
def abs(a:Double) = if(a<0) -a else a
def isCloseEnough(guess:Double, f : Double=>Double ) = 
    abs(f(guess)-guess)<0.00001
def FixedPoint(f: Double=>Double,initialGuess:Double ) : Double = {
    def find(guess:Double):Double= {
        if(isCloseEnough(guess,f)) guess
        else find(f(guess))
    }
    find(initialGuess)
}

//square root function for checking if code above is correct
//Looks like it's correct. Working for root. Will try for cube root later.

def sqroot( x:Double, guess:Double ): Double = {
    def isGoodEnough(guess:Double, x: Double) = 
        abs(square(guess)-x)<0.001
    def abs(x:Double) = {
        if(x>0) x
        else -x
    }
    def square( x : Double ) = x*x
    
    def improve(guess:Double, x:Double) = ( guess+x/guess )/2    
    
    if( isGoodEnough(guess,x) ) 
        guess
    else
        sqroot(x,improve( guess,x ))
}
def root( x: Double ) : Double =
    sqroot(x,1.0)


//checking fixed point    
println(FixedPoint(root, 5))
