// a program to find the square root of a number

def root( x:Double, guess:Double ): Double = {
    def isGoodEnough(guess:Double, x: Double) = 
        abs(square(guess)-x)<0.001
    def abs(x:Double) = {
        if(x>0) x
        else -x
    }
    def square( x : Double ) = 
        x*x
    def improve(guess:Double, x:Double) = 
        ( guess+x/guess )/2    
    if( isGoodEnough(guess,x) )
        guess
    else
        root(x,improve( guess,x ))
}
def root( x: Double ) : Double =
    root(x,1.0)

println(root(10))
//will implement inputting a number when I learn it.
