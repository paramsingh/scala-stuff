def fact(i: Int): Int= {
    if( i==1 || i==0 )
        1
    else
        i * fact(i-1)
}

def tailRecFact(i:BigInt): BigInt = {
    def factAccumulator( accum : BigInt, i: BigInt) : BigInt = {
        if ( i==1 ) accum
        else factAccumulator(accum*i, i-1)
    }
    factAccumulator(1,i)
}

def isSpecial(i:Int) = {
    var dup : Int = i
    var sum : Int = 0
    while( dup != 0){
        sum = sum + fact(dup%10)
        dup = dup/10
    }
    sum == i
} 

if( isSpecial(145) )
    println("145 is special");
println(tailRecFact(5))

/** Write a function product that computes the product of the values of
 * functions at points over a given range.
 */

def product( f : Int => Int ) : (Int,Int)=>Int = {
    def productF(a: Int, b: Int):Int = {
        if( a==b ) f(a)
        else f(a)*productF(a+1,b)
    }
    productF
}

/**Write factorial in terms of product.*/

def factorial(i:Int) = product(x=>x)(1,i)
println(factorial(4));
